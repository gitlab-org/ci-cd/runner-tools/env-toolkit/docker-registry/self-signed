# Create self signed certificate

## Prerequisites

- Docker
- openssl

## Run

```shell
cp openssl.conf.example openssl.conf
# Add any hostnames under `alt_names` in openssl.conf, for example `IP.2 = 192.168.190.160`
./up.sh
```

## Cleanup

```shell
./down.sh
```

## Resources used

Generate self signed certificate: https://docs.docker.com/registry/insecure/#use-self-signed-certificates
Start registry with TLS: https://docs.docker.com/registry/deploying/#get-a-certificate

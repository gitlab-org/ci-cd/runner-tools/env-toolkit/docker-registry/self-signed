#!/usr/bin/env bash

mkdir -p certs

echo "============================"
echo "  Create self-signed certs  "
echo "============================"

openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key \
  -x509 -days 365 -out certs/domain.crt -config openssl.conf


echo "============================"
echo "    Start Docker registry   "
echo "============================"

docker run -d \
  --name registry \
  -v "$(pwd)"/certs:/certs \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -p 443:443 \
  registry:2
